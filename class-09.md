# MFE 9815<p>Software Engineering in Finance<br/>9<br/>Norman Kabir _Autonomy Capital Research_<br/>Alain Ledon _Ally Financial_</p>
---

# Today We'll Cover

* Review of CRC Models
    * Agent
    * Asset
    * Market 
    * Serial Date
* Implementation
* User Stories
* New Financial Entities
    * Loans
    * Deposits

---

# Review of Serial Date CRC

---

# Review of Asset CRC

---

# Review of Agent CRC

---

# Summary of the Axioms

* Money is desirable
* More money is better than less
* Money now is better than money later
* Uncertainty is BAD

## If any of these axioms is going to be broken, we need to get compensated.

---

# User Stories

A USER STORY is a narrative that a user tells from her own perspective. This story typically describes some kind of scenario or situation that the user finds herself in. 

The developer must then work with the user to derive from that narrative the specific USER TASKS that the user must to perform with the software to successfully accomplish the story.

User stories are very slim and high-level requirements artifacts.

---

# Modeling with 2 Assets

We only have 2 assets *olives* and *money*. 

_User Story:_ 

+ I (a farmer) need money now but I will have olives on the future. 
+ You have money and you can give it to me (transfer of ownership). 

How much can I get to in return for the olives I will produce in the future?

We can look at the DF Curve from the previous class and determine the value of the olives/money today. This is how much money I can get today for olives in the future.

How can I find out how many olives I can exchange for money today?

---

# Modeling with 2 Assets

_User Story:_ 

+ I know the value of my olives in the future.
+ I want to get money today for use until some time T in the future.

How much money should I give you back at T to compensate you for violating Axiom 3?

In other words, How many olives should I give you in the future to use your money today?

This is clearly a _Transfer of Possesion_ operation and we are going to call it a *Loan*.

---

# Modeling with 2 Assets

_User Story:_ 

+ I don't need olives
+ I have money
+ I want more money (Axiom 1).
+ What should I do?

I need to give up utility/posession today in order to get more later. The amount I will get is the inverse of the DF presented in last class. This is another _Transfer of Possesion_ operation and we are going to call it a *Deposit*.

---

# Review of Market CRC

## Agent Interaction
* A **Market** is a legal environment in which **Agents** interact with other **Agents**.
* **Agents** interact to satisfy their **Utility Functions**.
* An **Asset** is anything that may be transferred among **Agents** in a **Market**.

---

# Market as a Singleton

The **Singleton** pattern is a design pattern used to implement the mathematical concept of a singleton, by restricting the instantiation of a class to one object. This is useful when exactly one object is needed to coordinate actions across the system. 

The concept is sometimes generalized to systems that operate more efficiently when only one object exists, or that restrict the instantiation to a certain number of objects. Also, they should be used with care in multithreaded environments.

There are criticism to the use of the singleton pattern because it is overused, introduces unnecessary restrictions in situations where a sole instance of a class is not actually required, and introduces global state into an application. ([Singletons are Evil](http://c2.com/cgi/wiki?SingletonsAreEvil "Singletons are Evil"))

However, **Singletons** are often preferred to global variables because:

* They don't pollute the global or containing name space with unnecessary variables.
* They permit lazy allocation and initialization, whereas global variables in many languages will always consume resources.

---
# Singleton Pattern UML

![Singleton UML] (http://dl.dropbox.com/u/1270571/500px-Singleton_UML_class_diagram.svg.png)

---
# Implementation in C++

    !cpp
    class Market {
    
    public:
      static Market& GetInstance();
      ...
    private:
      static Market* pMarket;
      Market();
      Market( const Market& );
      Market& operator=( const Market& );
    }; 
    
    Market& Market::GetInstance() {
    
      if( !pMarket ) {
        pMarket = new Market;
      }
      return *pMarket;
    }    

---

# Rule of Three in C++

C++ give you for free a Destructor, a copy constructor and the  assignment operator _=_ 

* The _free_ destructor will only clean the local object. Nothing else
* The _free_ copy constructor will create a shallow copy of your object
* The _free_ assignment operator will create a shallow copy of your object. Also, it won't do any clean up if needed.

You might not need to use any of them. However, if you do and you are not careful, it could lead to bad consequences.

If you need to write any of them in your class, you should (I would say _must_) write all THREE of them.

---

# Clean solution using Templates

Mark Joshi offers a clean solution for this problem by using templates:

    !cpp
    //
    //                              wrapper.h
    //
    #ifndef WRAPPER_H
    #define WRAPPER_H

    template< class T>
    class Wrapper
    {
    public:
        Wrapper() { DataPtr =0; }
    
        Wrapper(const T& inner) {
            DataPtr = inner.clone();
        }
    
        ~Wrapper() {
            if (DataPtr !=0)
                delete DataPtr;
        }
      
---

# Clean solution using Templates

    !cpp
        Wrapper(const Wrapper<T>& original) {
            if (original.DataPtr !=0)
                DataPtr = original.DataPtr->clone();
            else
                DataPtr=0;
        }
        
        Wrapper& operator=(const Wrapper<T>& original) {
            if (this != &original) {
                if (DataPtr!=0)
                    delete DataPtr;
                DataPtr = (original.DataPtr !=0) ? original.DataPtr->clone() : 0;
            }
            return *this;
        }
    
        T& operator*() {
            return *DataPtr; 
        }
    
        const T& operator*() const {
            return *DataPtr; 
        }

---

# Clean solution using Templates

    !cpp    
        const T* const operator->() const {
            return DataPtr;
        }
    
        T* operator->(){
            return DataPtr;
        }
    
    private:
        T* DataPtr;
    
    };
    #endif
    
---

# Usage

    !cpp
    class MyClass {
    public:
        MyClass* clone(){
            ....
        }
        ....
        
    }
    ...
    int main(...){
    
        Wrapper<MyClass> instance;
        
    	....
    }


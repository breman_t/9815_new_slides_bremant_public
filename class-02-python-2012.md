# MFE 9815<p>Software Engineering in Finance<br/>Class 2 - Python I<br/>Norman Kabir _Autonomy Capital Research_<br/>Alain Ledon _Ally Financial_</p>

---
# Today We'll Cover

* Python Language Basics
* Package System
* Python Shells
* PyDev

---
# An Interactive Shell

## Three common interactive python shells

* python
* [bpython](http://bpython-interpreter.org/)
* [IPython](http://ipython.org/)

## How to install and run

    !bash
    # To install bpython and IPython - Part of First Installation
    $ sudo apt-get install bpython
    $ sudo apt-get install ipython
    # to start bpython/IPython
    $ bpython
    >>> import this
    $ ipython
    

This is known as [Read Eval Print Loop](http://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop) (REPL). **You'll get immediate feedback.**

---
# Python Type System*

.notes: [Python website](http://www.python.org)

## Python is an extremely versatile, multi-paradigm language

It is dynamic - _variables may be declared without a type_

    !python
    mystr = "this is a string"
    mynum = 213

Typing is strong - _restricts how operations involving different types can be combined_

    !python
    >>> a = 123
    >>> b = "a string"
    >>> a + b
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
      TypeError: unsupported operand type(s) for +: 'int' and 'str'


Python is case-sensitive `myvar` and `MYVAR` are distinct.

    !python
    >>> myvar = 23
    >>> MYVAR = 3
    >>> print myvar + MYVAR
    26

---
# Comparison of Type Systems*

.notes: [Type systems](http://en.wikipedia.org/wiki/Type_system) 

## C++ and Java
<font color="maroon">Static and Strong</font>

## Python
<font color="navy">Dynamic and Strong</font>

## Our selection of languages was not arbitrary.

Large systems with large teams tend to use staticly typed languages.

_But you will rarely build large, complex system._

More often, you'll _interact_ with large, complex systems:

* Operating System and Libraries
* Servers on LAN and Internet

---
# Help

## Documentation available

    !python
    >>> import os
    >>> help(os)
    ...[ help output ]...

## Query an object's interface with

    !python
    >>> dir(os)
    ['EX_CANTCREAT', 'EX_CONFIG', 'EX_DATAERR', ... ] 

## You can also query an object's docstring

    !python
    >>> import os
    >>> os.__doc__
    "OS routines for Mac, NT, or Posix depending ... "

---
# Syntax

## Python is extremely readable

<font color="red">60% to 80% of software cost is in maintenance.</font>

## No mandatory statement termination characters

    !python
    myval = 3 + 6

_No terminating semicolon_

## Lexical scope (blocks) specified by indentation

    !python
    class Payment(object):
        def __init__(self):
            """
            A Multiline comment
            With additional documentation
            """
            self.amount = 1000  # Note no terminal semicolon
            self.currency = 'USD'   # Scope is indented

* One line comments begin with `#` 
* Multi-line comments begin with `"""` (single or double quotes)

---
# Data Types

Python comes with a rich collection of data structures

## Lists and Tuples (Immutable Lists)

Python lists may contain any type including other lists.

    !python
    >>> fruit = [ 'apple', 'banana' ]
    >>> color = [ 'red', 'yellow' ]
    >>> blend = [ fruit, color ]

Negative numbers count from the end. -1 is the last item.

    !python
    >>> mylist = [1,2,3,4,5]
    >>> mylist[0]
    1
    >>> mylist[-1]
    5

Tuples behave just like lists but are immutable.

    !python
    >>> mylist[0]=123
    >>> mylist
    [123, 2, 3, 4, 5]
    >>> mytuple = (1,2,3)
    >>> mytuple[0]=123
      TypeError: 'tuple' object does not support item assignment

---
# Data Types (cont'd)

## Slicing

Access slices of lists with the colon `:`

    !python
    >>> mylist=[1,2,3,4,5]
    >>> mylist[0:2]
    [1, 2]


Leave the start index blank to imply the first element, leave the end index blank to imply last.

    !python
    >>> mylist[:-1]
    [1, 2, 3, 4]

---
# Strings

## Quoting

Python strings can be single quoted, double quoted or triple quoted.

    !python
    >>> mystr = 'hello'
    >>> mystr2 = "hello"
    >>> mystr3 = "The computer said 'hello, world!'"

## Parameters

Parametrize string with `%` and tuples

    !python
    >>> greeting = "Hello, %s. We calculated the result %.3f" % ("Alice", 6.632)
    >>> print greeting
    Hello, Alice. We calculated the result 6.632

---
# Flow Control

Flow control statements include `if`, `for` and `while`. 

    !python
    >>> rangelist = range(10)
    >>> print rangelist
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    >>> help(range)

`range` supports _start_, _stop_, and _step_ parameters:

    !python
    >>> for i in range(3,10,2):
    ...     print i
    ... 
    3
    5
    7
    9

Python also supports _list comprehension_.

    !python
    >>> words = 'The quick brown fox jumps over the lazy dog'.split()
    >>> collection = [[w.upper(), w.lower(), len(w)] for w in words]
    >>> print collection
    [['THE', 'the', 3], 
     ['QUICK', 'quick', 5], 
     ['BROWN', 'brown', 5], ...

---
# Flow Control (cont)

`if`, `else` and `elif`

    !python
    >>> x = 22
    >>> if x == 5:
            print "five"
        elif x == 22:
            print "twenty two"
        else:
            print "neither 5 nor 22"
            
    twenty two
    >>> # booleans
    >>> x = True
    >>> if X: print "YES"
    YES
    >>> if not X: print "YES"
        else: print "NO"
    NO
    
---
# Functions*

.notes: [Lambda functions](http://www.secnetix.de/olli/Python/lambda_functions.hawk)

Declare functions with the `def` keyword:

    !python
    >>> def square(val):
    ...     return val * val;
    ...     
    ... 
    >>> print square(32)
    1024

Parameters are passed by reference.

Python supports anonymous functions that can be created at runtime.

Lambda functions aren't quite the same as lambda in functional programming but they are powerful constructs in their own right.

    !python
    >>> def make_incrementor (n): return lambda x: x + n
    ... 
    >>> f = make_incrementor(25)
    >>> g = make_incrementor(100)
    >>> print f(42), g(42)
    67 142

---
# More about functions

    !python
    In [26]: def do_something(a, b, x=0):
        ....:     """Calculates b / a - x"""
        ....:     return b / a - x
        ....:
        
    In [27]: help(do_something)
    Help on function do_something in module __main__:

    do_something(a, b, x=0)
        Calculates b / a - x
    
    In [22]: do_something(4, 2, 5)
    Out[22]: -5
    
    In [23]: do_something(4, 2)
    Out[23]: 0
    
    In [24]: do_something(4., 2.)
    Out[24]: 0.5
    
    In [25]: do_something(4., x=2., b=3)
    Out[25]: -1.25

---
# References

* [A Byte of Python](http://www.ibiblio.org/swaroopch/byteofpython/read/)
* [Learn Python The Hard Way](http://learnpythonthehardway.org/book/)
* [Free Python Books](http://pythonbooks.revolunet.com/)
* [PEP 8 -- Style Guide for Python Code](http://www.python.org/dev/peps/pep-0008/)
* [Code Like a Pythonista: Idiomatic Python](http://python.net/~goodger/projects/pycon/2007/idiomatic/handout.html)
* [Python 2.x Documentation](http://docs.python.org/)
* [Google python](http://lmgtfy.com/?q=python)

---

# Importing modules

Python has an enormous collection of libraries. For example

* Built-in modules - [PyMOTW](http://www.doughellmann.com/PyMOTW/genindex.html)
* [NumPy](http://numpy.scipy.org/) - linear algebra, Fourier transform, and C++/Fortran integration (today)
* [SciPy](http://scipy.org/) - Extends NumPy with optimization, ODE solvers, signal processing (later)
* [pandas](http://pandas.pydata.org/) - Practical, real-world statistical analysis (hopefully "Straight from the horse's mouth")
* [xlrd](https://secure.simplistix.co.uk/svn/xlrd/trunk/xlrd/doc/xlrd.html) - for reading excel files
* much more... [PyPi](http://pypi.python.org/pypi)

To install a package on your system

    !bash
    $ sudo pip install scipy
    # if you are behind a proxy
    $ sudo pip install --proxy <proxy url> <package name>

To update a package

    !bash
    $ sudo pip install -u scipy
    
---
# More about modules

Remember to explore what others have written before writing something from scratch!

    !python
    >>> import random
    >>> randomint = random.randint(1,500)
    >>> print randomint
    141
    >>> from datetime import date
    >>> print date.today()
    datetime.date(2012, 9, 5)
    >>> from datetime import timedelta as td
    >>> date.today() + td(days=10)
    datetime.date(2012, 9, 15)

---
# File I/O

Accessing the file system with Python is _easy_!

    !python
    >>> myfile = open("/tmp/data.txt","w")
    >>> myfile.write("this is some important data")
    >>> myfile.close()
    >>> # let's read it
    >>> readfile = open("/tmp/data.txt","r")
    >>> print readfile.readlines()
    ['this is some important data']
    >>> readfile.close()

---
# Python Shells

.notes: [Read, evaluate, print loop](http://en.wikipedia.org/wiki/Read-eval-print_loop)

Python has two excellent shells for REPL (Read, Evaluate, Print loop).

## IPython

* Interactive data visualization (graphing)
* Parallel computing
* Extensive inline help system

## bpython 

* In-line syntax highlighting
* Autocomplete
* Parameter hints
* "Rewind" function
* Share snippets via Pastebin
---
# Python and Eclipse

Eclipse supports a full environment for Python development, pydev.

[http://pydev.org/](http://pydev.org/)

It includes:

* Code completion
* Syntax highlighting
* Code Analysis
* Debugging
* Interactive Console
* Unit testing and more...

Install it using Eclipse Update Manager using this link:

[http://pydev.org/updates](http://pydev.org/updates)

---
# Eclipse Installation*
.notes: [from Baruch MFE Forum](http://mfeapp.baruch.cuny.edu/forum/threads/preparation-for-class-6.7799/)

Create a snapshot of the VM. This is always a good idea. Then, issue the following commands to install Sun JDK

	!bash
	$ sudo apt-get update
	$ sudo apt-get install -y openjdk-7-jdk openjdk-7-jre

(You might need to accept the license (press Tab, Enter, Tab on Yes then).

Download Eclipse for your platform [Eclipse C++](http://www.eclipse.org/downloads/packages/eclipse-ide-cc-developers-includes-incubating-components/indigosr1). It automatically downloads to <em>~/Downloads</em> <br>

Open a terminal and expand the tar ball that was downloaded

	!bash
	$ cd ~/Download
	$ tar xzf <eclipse...tar.gz>
	
To execute eclipse:

	!bash
	$ ~/Download/eclipse/eclipse

You can move **eclipse** anywhere at this point.

---
# PyDev - Configure Interpreter*

.notes: [http://pydev.org/manual_101_interpreter.html](http://pydev.org/manual_101_interpreter.html)

From inside Eclipse go to **Window** > **Preferences** > **PyDev** > **Interpreter - (Python/Jython/IronPython)**

Click on **Auto Config** and Eclipse will detect the current Python interpreter location.

If this fails, you can find the current pytho interpreter from the terminal

    !bash
    !which python
    
Click **Ok** when done.

---
# PyDev - Creating a Project*

.notes: [http://pydev.org/manual_101_project_conf.html](http://pydev.org/manual_101_project_conf.html)

From inside Eclipse go to **File** > **New** > **Project** > **PyDev** > **PyDev Project**

**Project name**: this is the name of the project.

**Project contents**: where it should be located.

**Project type**: defines the set of interpreters that will be available for that project.

**Grammar version**: the grammar used for parsing the files in this project. It doesn't have to match the grammar of the actual interpreter selected. This is done so that you can use a newer interpreter while having the grammar you use compatible with an older interpreter (e.g.: using a Python 2.6 interpreter with a Python 2.4 grammar in the project).

**Interpreter**: Defines which interpreter should be used for code-completion and default run configurations (note that you can still create a run configuration with a different interpreter).

**Create default 'src' folder and add it to the pythonpath**: If you don't leave that option checked, you'll have to create the source folder(s) yourself after the project is created (which is covered in the next tutorial page).

You may finish in this screen or go to the next, where you will be asked which projects are referenced from this one. Click **Finish** if you want to go back to the editor.

---
# PyDev - Runnning a Python Script*

.notes: [http://pydev.org/manual_101_run.html](http://pydev.org/manual_101_run.html)

Let's add some code and run it

    !python
    if __name__ == '__main__':
        print 'Hello World'

Running the Python script is very similar to run any program from inside Eclipse (like the refresher).

* Use a shortcut: <em>F9</em> to run based on the project configuration where the module is contained.
* Go to the menu: <em>Alt + R + S + The number of the Run</em> you wish (It can be Python, Jython, unit-test, etc).
* Right Click on the <em>.py</em> file and click on <em>Run as</em> > <em>Python Run</em>

---
# Introduction to Numpy

[http://numpy.scipy.org/](http://numpy.scipy.org/)

* A powerful N-dimensional array object (ndarray)
* Sophisticated (broadcasting) functions
* Tools for integrating C/C++ and Fortran code
* Useful linear algebra, Fourier transform, and random number capabilities

## Installing numpy

    !bash
    $ sudo pip install numpy
    
---
# NumPy Examples

## Creating vectors (ndarrays) and matrices:

	!python
	>>> import numpy as np
	>>> np.arange(10)
	array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
	>>> np.zeros((3,3))
	array([[ 0., 0., 0.],
	       [ 0., 0., 0.],
	       [ 0., 0., 0.]])
	>>> np.ones((3,2))
	array([[ 1., 1.],
	       [ 1., 1.],
	       [ 1., 1.]])
	>>> np.empty((2,2))
	array([[ 0.00000000e+000, 4.22653002e-317],
	       [ 2.45625605e-316, 6.94925844e-310]])
           
---
# NumPy Examples (cont.)

Creating random vectors (ndarrays) and matrices:

	!python
	>>> np.random.randn(4)
	array([-1.13385833, 1.01105521, 0.10653996, -1.12119788])
	>>> np.random.randn(2,2)
	array([[ 1.58729301, -0.43556406],
	       [ 0.55324542,  0.53574787]])
	>>> np.random.randint(3)
	2
	>>> np.random.rand(3)
	array([ 0.46455155, 0.56234086, 0.64087288])

---
# NumPy Operations

	!python
	>>> np.arange(5)**2
	array([ 0, 1, 4, 9, 16])
	>>> a = np.arange(4).reshape(2,2)
	>>> a
	array([[0, 1],
	       [2, 3]])
	>>> b = np.ones((2,2))
	>>> b
	array([[ 1.,  1.],
	       [ 1.,  1.]])
	>>> a * b
	array([[ 0., 1.],
	       [ 2., 3.]])
	>>> np.dot(a, b)
	array([[ 1., 1.],
	       [ 5., 5.]])

---
# NumPy Operations

	!python
	>>> b *= 2
	>>> b
	array([[ 2., 2.],
	       [ 2., 2.]])
	>>> b + a
	array([[ 2., 3.],
	       [ 4., 5.]])
	>>> b = np.linspace(0, np.pi, 3)
	>>> b
	array([ 0. , 1.57079633, 3.14159265])
	>>> a
	array([[0, 1],
	       [2, 3]])
	>>> a[1,1]
	3
	>>> a[:,1]
	array([1, 3])
	>>> a[0,:]
	array([0, 1])

---
# NumPy Operations

	!python
	>>> a = np.arange(16)
	>>> a
	array([ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
	>>> a.reshape(2,8)
	array([[ 0, 1, 2, 3, 4, 5, 6, 7],
	       [ 8, 9, 10, 11, 12, 13, 14, 15]])
	>>> a.reshape(4,4)
	array([[ 0, 1, 2, 3],
	       [ 4, 5, 6, 7],
	       [ 8, 9, 10, 11],
	       [12, 13, 14, 15]])
	>>> c = a.reshape(4,4).copy()

---
# NumPy Operations
    
    !python
    >>> c.transpose()
    array([[ 0, 4, 8, 12],
           [ 1, 5, 9, 13],
           [ 2, 6, 10, 14],
           [ 3, 7, 11, 15]])
     >>> c[1:3]
     array([[ 4, 5, 6, 7],
            [ 8, 9, 10, 11]])
     >>> k = c > 7
     >>> c[k]
     array([ 8, 9, 10, 11, 12, 13, 14, 15])

---
# NumPy Linear Algebra Operations

  	!python
   	>>> import numpy.linalg as npl
   	>>> c = np.array([1, 0, -2, 4, 1, 0, 1, 1, 7]).reshape(3, 3)
   	>>> c
   	array([[ 1, 0, -2],
	       [ 4, 1, 0],
               [ 1, 1, 7]])
  	>>> npl.inv(c)
   	array([[  7., -2.,  2.],
          	[-28.,  9., -8.],
          	[  3., -1.,  1.]])
   	>>> b = c[:3,:3]
   	>>> npl.eig(b)
   	(array([ 1.64582364e+01, -1.45823643e+00, -2.47157218e-17]), array([[ 0.13438298, 0.78439438, 0.40824829],
          	[ 0.49609457, 0.08499126, -0.81649658],
          	[ 0.85780615, 0.61441186,  0.40824829]]))
   	>>> y = np.array([3, 5, 6])
	>>> npl.solve(c, y)
	array([ 23., -87., 10.])

---
# NumPy References

* [http://www.scipy.org/Tentative_NumPy_Tutorial](http://www.scipy.org/Tentative_NumPy_Tutorial)
* [http://www.scipy.org/Numpy_Example_List](http://www.scipy.org/Numpy_Example_List)
* [http://www.tramy.us/](http://www.tramy.us/)
* [http://mathesaurus.sourceforge.net/](http://mathesaurus.sourceforge.net/)

---
# Clone the Demo Project

    !bash
    $ mkdir ${HOME}/hgdev/org.bitbucket/b9815
    $ cd ${HOME}/hgdev/org.bitbucket/b9815
    $ hg clone https://${USER}@bitbucket.org/b9815/python

<h2>You now have the demo source code installed locally</h2>

---
# Work With Data

    !bash
    # start your ipython session
    $ cd ${HOME}/hgdev/org.bitbucket/b9815/python
    $ ./isession.sh

## Let's get Google's stock price

    !python
    In [1]: import ystockquote
    In [2]: ystockquote.get_price('GOOG')
    Out[2]: '680.72'

---

# Basic Visualization

## From within the same folder

    !bash
    $ cd ${HOME}/hgdev/org.bitbucket/b9815/python
    $ ipython --pylab
    ...
    In [1]: %run goog-chart.py
    
    In [2]: # inside the same session
    
    In [3]: %run linked-chart.py

    In [4]:
    

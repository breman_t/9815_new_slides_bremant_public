# MFE 9815<p>Software Engineering in Finance<br/>11<br/>Norman Kabir _Autonomy Capital Research_<br/>Alain Ledon _Ally Financial_</p>
---

# Today We'll Cover

* Review of CRC Models
    * Bonds
    * Zero Coupon Bonds or Principal Only Bond = One Payment
    * Bullet Bond or Fixed Coupon Bond = Zero Coupon Bond + Annuity 
    * Floating Rate Bond
    * Forward Rate Agreement
    * Forward
    * Swaps
* Code Review

---

# Asset Interface
	
	!python
	class Asset(object):
	    '''
	    from Jie/Dru
	    
	    id = unique identifier for an asset
	    name = human-friendly name
	    '''
	    def __init__(self, id_number, name):
	    	self.id = id_number
		self.name = name
	    '''
	    Present value of an asset received on a future date
	    '''
	    def pv(self, market, slice_date): 
	    	pass
	    '''
	    Future value of an asset received on a future date
	    '''
	    def fv(self, market, slice_date):
	    	pass   

---

# Annuity

---

# Bonds

A Bond is a tradeable asset that represents a *Loan*.

---

# Zero Coupon Bond

A *Loan* where you will get all the principal at maturity T.

---

# Fixed Coupon Bond (Bullet Bond)

We can think of it as a *Zero Coupon Bond* that pays principal at maturity and an *Annuity*.

---

# Floating Rate Bond

Parameterized Compensation because growth rates change (DF changes) and I want to avoid uncertainty. On reset day the loan goes to par.

---

# Forward Rate Agreement

Negotiated Asset, agree to make a loan at T1 and get paid principal at T2 > T1. 

---

# Forwards

Use Case: I want to buy olives at some point in the future and, due to Axiom 4, I want to avoid uncertainty on the price of the olives. 

To avoid that uncertainty, I enter into a Forward.

---

# Swaps

Use Case: I need olives a multiple time points in the future. I would like to enter in a series of Forward Rate Agreements. 

We can package them alltogether in a single tradeble asset, the Swap.

---

# CRC Design

---

# More Code Review

---


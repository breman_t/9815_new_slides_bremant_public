# MFE 9815<p>Software Engineering in Finance<br/>4<br/>Norman Kabir _Autonomy Capital Research_<br/>Alain Ledon _Ally Financial_</p>

---

# Introduction to Python Libraries 

## Numerical and Scientific Libraries

* NumPy: Fundamental package needed for scientific computing with Python
* SciPy: Collection of mathematical algorithms and convenience functions built on NumPy
* matplotlib: 2D plotting library for interactive use
* pandas: Power data analysis toolkit similar to R

---

# Introduction to Numpy

[http://numpy.scipy.org/](http://numpy.scipy.org/)

* A powerful N-dimensional array object (ndarray)
* Sophisticated (broadcasting) functions
* Tools for integrating C/C++ and Fortran code
* Useful linear algebra, Fourier transform, and random number capabilities.

## Installing numpy

	!bash
	$ sudo apt-get install python-numpy

---

# NumPy Examples

## Creating vectors (ndarrays) and matrices:

	!python
	>>> import numpy as np
	>>> np.arange(10)
	array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
	>>> np.zeros((3,3))
	array([[ 0., 0., 0.],
	       [ 0., 0., 0.],
	       [ 0., 0., 0.]])
	>>> np.ones((3,2))
	array([[ 1., 1.],
	       [ 1., 1.],
	       [ 1., 1.]])
	>>> np.empty((2,2))
	array([[ 0.00000000e+000, 4.22653002e-317],
	       [ 2.45625605e-316, 6.94925844e-310]])

---

# NumPy Examples (cont.)

Creating random vectors (ndarrays) and matrices:

	!python
	>>> np.random.randn(4)
	array([-1.13385833, 1.01105521, 0.10653996, -1.12119788])
	>>> np.random.randn(2,2)
	array([[ 1.58729301, -0.43556406],
	       [ 0.55324542,  0.53574787]])
	>>> np.random.randint(3)
	2
	>>> np.random.rand(3)
	array([ 0.46455155, 0.56234086, 0.64087288])

---

# NumPy Operations

	!python
	>>> np.arange(5)**2
	array([ 0, 1, 4, 9, 16])
	>>> a = np.arange(4).reshape(2,2)
	>>> a
	array([[0, 1],
	       [2, 3]])
	>>> b = np.ones((2,2))
	>>> b
	array([[ 1.,  1.],
	       [ 1.,  1.]])
	>>> a * b
	array([[ 0., 1.],
	       [ 2., 3.]])
	>>> np.dot(a, b)
	array([[ 1., 1.],
	       [ 5., 5.]])

---

# NumPy Operations

	!python
	>>> b *= 2
	>>> b
	array([[ 2., 2.],
	       [ 2., 2.]])
	>>> b + a
	array([[ 2., 3.],
	       [ 4., 5.]])
	>>> b = np.linspace(0, np.pi, 3)
	>>> b
	array([ 0. , 1.57079633, 3.14159265])
	>>> a
	array([[0, 1],
	       [2, 3]])
	>>> a[1,1]
	3
	>>> a[:,1]
	array([1, 3])
	>>> a[0,:]
	array([0, 1])

---

# NumPy Operations

	!python
	>>> a = np.arange(16)
	>>> a
	array([ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
	>>> a.reshape(2,8)
	array([[ 0, 1, 2, 3, 4, 5, 6, 7],
	       [ 8, 9, 10, 11, 12, 13, 14, 15]])
	>>> a.reshape(4,4)
	array([[ 0, 1, 2, 3],
	       [ 4, 5, 6, 7],
	       [ 8, 9, 10, 11],
	       [12, 13, 14, 15]])
	>>> c = a.reshape(4,4).copy()
---

# NumPy Operations
    
    !python
    >>> c.transpose()
    array([[ 0, 4, 8, 12],
           [ 1, 5, 9, 13],
           [ 2, 6, 10, 14],
           [ 3, 7, 11, 15]])
     >>> c[1:3]
     array([[ 4, 5, 6, 7],
            [ 8, 9, 10, 11]])
     >>> k = c > 7
     >>> c[k]
     array([ 8, 9, 10, 11, 12, 13, 14, 15])

---

# NumPy Linear Algebra Operations

  	!python
   	>>> import numpy.linalg as npl
   	>>> c = np.array([1, 0, -2, 4, 1, 0, 1, 1, 7]).reshape(3, 3)
   	>>> c
   	array([[ 1, 0, -2],
	       [ 4, 1, 0],
               [ 1, 1, 7]])
  	>>> npl.inv(c)
   	array([[  7., -2.,  2.],
          	[-28.,  9., -8.],
          	[  3., -1.,  1.]])
   	>>> b = c[:3,:3]
   	>>> npl.eig(b)
   	(array([ 1.64582364e+01, -1.45823643e+00, -2.47157218e-17]), array([[ 0.13438298, 0.78439438, 0.40824829],
          	[ 0.49609457, 0.08499126, -0.81649658],
          	[ 0.85780615, 0.61441186,  0.40824829]]))
   	>>> y = np.array([3, 5, 6])
	>>> npl.solve(c, y)
	array([ 23., -87., 10.])

---

# NumPy References

* [http://www.scipy.org/Tentative_NumPy_Tutorial](http://www.scipy.org/Tentative_NumPy_Tutorial)
* [http://www.scipy.org/Numpy_Example_List](http://www.scipy.org/Numpy_Example_List)
* [http://www.tramy.us/](http://www.tramy.us/)
* [http://mathesaurus.sourceforge.net/](http://mathesaurus.sourceforge.net/)
* [http://docs.scipy.org/doc/](http://docs.scipy.org/doc/)
* [http://www.scipy.org/Cookbook](http://www.scipy.org/Cookbook)

---

# Introduction to SciPy

SciPy is a collection of mathematical algorithms and convenience functions built on the Numpy extension for Python. It adds significant power to the interactive Python session by exposing the user to high-level commands and classes for the manipulation and visualization of data. With SciPy, an interactive Python session becomes a data-processing and system-prototyping environment rivaling sytems such as MATLAB, IDL, Octave, R-Lab, and SciLab.

## Installing SciPy

	!bash
	$ sudo apt-get install python-scipy

---

# SciPy Examples - Linear Algebra

	!python
	>>> import scipy as sp
	>>> from scipy import linalg
	>>> C = sp.mat(c)
	>>> C
	matrix([[ 1, 0, -2],
	        [ 4, 1, 0],
	        [ 1, 1, 7]])
	>>> linalg.inv(c)
	array([[  7., -2.,  2.],
	       [-28.,  9., -8.],
	       [  3., -1.,  1.]])
	>>> linalg.inv(C)
	array([[  7., -2.,  2.],
	       [-28.,  9., -8.],
	       [  3., -1.,  1.]])
	>>> la,v = linalg.eig(C)
	>>> la
	array([ 0.06076513+0.j, 2.59327157+0.j, 6.34596330+0.j])
	>>> v
	array([[ 0.22728567, -0.35494198, -0.33894067],
               [-0.96796094, -0.89110228, -0.25360494],
	       [ 0.10673731,  0.28275948, 0.9059822 ]])

[http://docs.scipy.org/doc/scipy/reference/tutorial/linalg.html](http://docs.scipy.org/doc/scipy/reference/tutorial/linalg.html

---

# SciPy Examples - Statistics

    !python
    >>> from scipy import stats
    >>> discrete = [d for d in dir(stats) if isinstance(getattr(stats,d), stats.rv_discrete)]
    >>> discrete
    ['bernoulli', 'binom', 'boltzmann', 'dlaplace', 'geom', 'hypergeom', 'logser', 'nbinom', 'planck', 'poisson', 'randint', 'zipf']
    >>> continu = [d for d in dir(stats) if isinstance(getattr(stats,d), stats.rv_continuous)]
    >>> np.random.seed(12345)
    >>> stdnorm = stats.norm(0, 1)
    >>> stdnorm.rvs(5)
    array([-0.20470766, 0.47894334, -0.51943872, -0.5557303 , 1.96578057])
    >>> stdsample.var()
    0.98968997027310834
    >>> stdsample.mean()
    -0.00012350571773369349
    >>> stdsample.std()
    0.99483162910771405
    >>> np.random.seed(12345)
    >>> np.random.normal(size=5)
    array([-0.20470766, 0.47894334, -0.51943872, -0.5557303 , 1.96578057])


[http://docs.scipy.org/doc/scipy/reference/tutorial/stats.html](http://docs.scipy.org/doc/scipy/reference/tutorial/stats.html)

---

# SciPy Examples - Interpolation

Start IPython with pylab

	!bash
	$ ipython -pylab

In IPython type:

	!python
	>>> from scipy.interpolate import interp1d
	>>> x = np.linspace(0, 10, 10)
	>>> y = np.exp(-x/3.0)
	>>> f = interp1d(x, y)
	>>> f2 = interp1d(x, y, kind='cubic')
	>>> xnew = np.linspace(0, 10, 40)
	>>> import matplotlib.pyplot as plt
	>>> plt.plot(x,y,'o',xnew,f(xnew),'-', xnew, f2(xnew),'--')
	>>> plt.legend(['data', 'linear', 'cubic'], loc='best')
	>>> plt.show()
	>>> x = np.arange(0,2*np.pi+np.pi/4,2*np.pi/8)
	>>> y = np.sin(x)
	>>> tck = interpolate.splrep(x,y,s=0)
	>>> xnew = np.arange(0,2*np.pi,np.pi/50)
	>>> ynew = interpolate.splev(xnew,tck,der=0)
	>>> plt.figure()
	>>> plt.plot(x,y,'x',xnew,ynew,xnew,np.sin(xnew),x,y,'b')
	>>> plt.legend(['Linear','Cubic Spline', 'True'])
	>>> plt.axis([-0.05,6.33,-1.05,1.05])
	>>> plt.title('Cubic-spline interpolation')

[http://docs.scipy.org/doc/scipy/reference/tutorial/interpolate.html](http://docs.scipy.org/doc/scipy/reference/tutorial/interpolate.html)

---

# SciPy - Other Packages

* Optimization
* Integration
* Signal processing
* Fourier transform
* more...

---

# Scipy References

- [http://docs.scipy.org/doc/](http://docs.scipy.org/doc/)
- [http://docs.scipy.org/doc/scipy/reference/tutorial/index.html](http://docs.scipy.org/doc/scipy/reference/tutorial/index.html)
- [http://www.scipy.org/Cookbook](http://www.scipy.org/Cookbook)

---

# Introduction to matplotlib

matplotlib is a python 2D plotting library which produces publication quality figures in a variety of hardcopy formats and interactive environments across platforms. matplotlib can be used in python scripts, the python and ipython shell (ala MATLAB® or Mathematica®), web application servers, and six graphical user interface toolkits.

## Installing matplotlib

	!bash
	$ sudo apt-get install python-matplotlib
	$ ipython -pylab

	!python
	In [27]: x = randn(10000)
	In [28]: hist(x, 100)

---

# matplotlib examples

	!python
	>>> x = mu + sigma * np.random.randn(10000)
	>>> # the histogram of the data
	>>> n, bins, patches = plt.hist(x, 50, normed=1, facecolor='g', alpha=0.75)
	>>> plt.xlabel('Smarts')
	>>> plt.ylabel('Probability')
	>>> plt.title('Histogram of IQ')
	>>> plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
	>>> plt.grid(True)





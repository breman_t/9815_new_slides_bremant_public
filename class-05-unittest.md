# Test Driven Development (TDD)

## Something that is untested is broken.

---
# Types of Testing

* **Unit Testing** - Test the smallest pieces of a program. Often, it means individual functions or methods. Make sure the unit tested works as expected.
* **Integration Testing** - Test interactions between related units. Check if tested units behave correctly as a group
* **System Testing** - Checks parts of the program after everything has been put together. They are like a extreme form of **Integration Testing**

We are going to cover **Unit Testing**

---
# Unit Tests

* Test functions/methods in isolation, independent units
* Find problems early in the development process
* Serve also as a living documentation for your code
* Tests life-cycle  
    - Write a test 
    - Watch it fail
    - Make it pass  (Refactor your code)
    - Rinse repeat  

---
# Unit Test Frameworks

[Python Unit Testing Tools](http://pycheesecake.org/wiki/PythonTestingToolsTaxonomy#UnitTestingTools)

Various unit test frameworks available in python. 

* [unittest](http://docs.python.org/library/unittest.html)
* [nose](http://nose.readthedocs.org/en/latest/)
* doctest
* pytest

... and many more  

---
# unittest - Python built-in framework

[Python unittest module](http://docs.python.org/library/unittest.html)

* Comes standard with Python
* Usually called PyUnit - Python version of JUnit Java test framework
* Supports test automation
* Based on 4 core concepts:
    * __Test Fixture__ - Preparation needed to perform one or more tests, and any associate cleanup actions
    * __Test Case__ - The smallest unit of testing. It checks for a specific response to a particular set of inputs
    * __Test Suite__ - Collection of test cases, test suites or both
    * __Test Runner__ - Executor of tests
* Test Case classes must inherit from **unittest.TestCase**
* Test functions and test modules should be named starting with **test**/**Test**

---
# unittest Example*

[https://aledon2@bitbucket.org/aledon2/mfe9815pythontdd](https://aledon2@bitbucket.org/aledon2/mfe9815pythontdd)

    !python
    # splitter.py - From Essential Python Reference
    def split(line, types=None, delimiter=None):
        """Splits a line of text and optionally performs type conversion
            ...
        """
        fields = line.split(delimiter)
        if types:
            fields = [ty(val) for ty, val in zip(types, fields)]
        return fields

---
# unittest Example (cont.)

    !python
    import splitter
    import unittest

    # Unit Tests
    class TestSplitFunction(unittest.TestCase):
        def setUp(self):
            # Perform set up (if any)
            pass
        def tearDown(self):
            # Perform clean up (if any)
            pass
        def test_simple_string(self):
            r = splitter.split('GOOG 100 701.50')
            self.assertEqual(r, ['GOOG', '100', '701.50'])
        def test_type_convert(self):
            r = splitter.split('GOOG 100 701.50', [str, int, float])
            self.assertEqual(r, ['GOOG', 100, 701.50])
        def test_delimiter(self):
            r = splitter.split('GOOG,100,701.50', delimiter=',')
            self.assertEqual(r, ['GOOG', '100', '701.50'])

    # Run the unittests
    if __name__ == '__main__':
        unittest.main()

---
# unittest functions

Some of the methods supported by **unittest.TestCase**:

* t.setUp
* t.tearDown
* t.assert_ / t.failIf
* t.assertEqual / t.failUnlessEqual
* t.assertNotEqual / t.failIfEqual
* t.assertAlmostEqual / t.failUnlessAlmostEqual
* t.assertRaises / t.failUnlessRaises

---
# Nosetests*

* [An Extended Introduction to the nose Unit Testing Framework](http://ivory.idyll.org/articles/nose-intro.html)
* Popular Python unit test framework
* Compatible with **unittest**
* Support advance features 
* Discovers and runs unit tests
* Anything that matches **((?:^|[b_.-])[Tt]est)** will be considered a test (i.e. don't use the word test in any name)
* Doesn't need a super class

Installation and Run:

    !bash
    $ sudo pip install nose
    $ nosetests
    ...

---
# Nosetests

* Nose looks through a directory's structure, finds the test files, sorts out the tests that they contain, runs the tests, and reports the results back to you. 
* Nose recognizes test files based on their names. Any file whose name contains test or Test either at the beginning or following any of the characters _, . or -
* Any directory whose name matches the same pattern is recognized as a directory that might contain tests, and so should be searched for test files.

---
# Nosetests

Some of the methods supported by **nose.tools**:

* t.assert_equal
* t.assert_not_equal
* t.assert_almost_equal
* t.assert_raises
* ...

Methods are very similar to **unittest.TestCase** but with a PEP-08 name convention.

---
# Nosetests Example

.notes: hg clone https://littlea1@bitbucket.org/littlea1/mfe9815python3

    !python
    import splitter
    from nose.tools import *

    # Unit Tests
    class TestSplitFunction():
        @classmethod
        def setup_class(klass):
            """This method is run once for each class before any tests are run"""
        @classmethod
        def teardown_class(klass):
            """This method is run once for each class _after_ all tests are run"""
        def setUp(self):
            """This method is run once before _each_ test method is executed"""
        def teardown(self):
            """This method is run once after _each_ test method is executed"""
        def test_simple_string(self):
            r = splitter.split('GOOG 100 701.50')
            assert_equal(r, ['GOOG', '100', '701.50'])
        def test_type_convert(self):
            r = splitter.split('GOOG 100 701.50', [str, int, float])
            assert_equal(r, ['GOOG', 100, 701.50])
        def test_delimiter(self):
            r = splitter.split('GOOG,100,701.50', delimiter=',')
            assert_equal(r, ['GOOG', '100', '701.50'])

--- 
# Nosetests

* Expandable by using plugins
* Supports test coverage by using coverage
* Supports profiling
* Supports debugging of tests online
* Supports parallel testing
* ... and much more (__$ nosetests --help__)

---
# Nosetests References

* [http://nose.readthedocs.org/en/latest/](http://nose.readthedocs.org/en/latest/)
* [http://nose.readthedocs.org/en/latest/writing_tests.html](http://nose.readthedocs.org/en/latest/writing_tests.html)
* [http://ivory.idyll.org/articles/nose-intro.html](http://ivory.idyll.org/articles/nose-intro.html)
* [http://bit.ly/QI0RS4](http://bit.ly/QI0RS4)

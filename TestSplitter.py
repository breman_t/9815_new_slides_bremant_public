import unittest
import splitter

# Unit Tests
class TestSplitFunction(unittest.TestCase):
    def setUp(self):
        # Perform set up (if any)
        pass
    def tearDown(self):
        # Perform clean up (if any)
        pass
    def test_simple_string(self):
        r = splitter.split('GOOG 100 701.50')
        self.assertEqual(r, ['GOOG', '100', '701.50'])
    def test_type_convert(self):
        r = splitter.split('GOOG 100 701.50', [str, int, float])
        self.assertEqual(r, ['GOOG', 100, 701.50])
    def test_delimiter(self):
        r = splitter.split('GOOG,100,701.50', delimiter=',')
        self.assertEqual(r, ['GOOG', '100', '701.50'])

if __name__ == "__main__":
    unittest.main()
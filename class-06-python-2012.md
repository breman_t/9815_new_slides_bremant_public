# MFE 9815<p>Software Engineering in Finance<br/>Class 6 - Python<br/>Norman Kabir _Autonomy Capital Research_<br/>Alain Ledon _Ally Financial_</p>

---
# Today We'll Cover

* Factories
* Reading and Writing Files
* Serialization in Python
* Reading Excel (if you have to)

---
# Factory Pattern

* Object-oriented creational design pattern 
* Control the object creation process
* Perform extra tasks needed in creating instances of an object

---
# Example*

.notes: hg clone https://littlea1@bitbucket.org/littlea1/mfe9815python3

    !python
    from shape import Shape

    class Circle(Shape):

        def __init__(self, name, radius):
            
            super(Circle, self).__init__(name)
            self.radius = radius

        def area(self):
            return 3.14159 * self.radius
            
        def anything_else(self):
            print "I have area = {0}".format(area)

        def __str__(self):
            return "Circle with name {0} and radius {1}".format(self.name, self.radius)

    def make(name, radius):
        if radius < 0:
            raise ValueError("Radius most be greater or equal to 0")
        try:
            r = float(radius)
            return Circle(name, float(radius))
        except ValueError as e:
            raise ValueError("Radius most be a number.")
        
    def make_empty(name):
        return Circle(name, 0.)

---
# More

    !python
    In [1]: import circle

    In [2]: c = circle.make("blue circle", 4.0)

    In [3]: print c
    Circle with name blue circle and radius 4.0

    In [4]: r = circle.make_empty("red circle")

    In [5]: print r
    Circle with name red circle and radius 0.0

    In [6]:

---
# Reading and Writing Files

* Reading and writing files is done using the function **open** 
* It follows the same semantics as old C **fopen**

Example:

    !python
    f = open('myfile', 'w') # open file for writing
    f.write("this is a test") # write something to the file
    f.write("something else") # write something else
    f.close() # close the file
    
    f = open('myfile', 'r') # open file for writing
    a = f.read(5) # read 5 bytes from a file
    a = f.readline()  # read one line (until \n)
    l = f.readlines()  # read all the lines and return a list
    # read all the lines in a loop
    for line in f:
        print line

---
# Reading and Writing Files

* It is a good practice to use the **with** keyword when dealing with files. 
* The file will be properly closed after its suite finishes, even if an exception is raised on the way.

Example:
    
    !python
    with open('myfile', 'r') as f:
        # process the file here 
        read_data = f.read()

---
# More about reading files

* If you want to process files efficiently use the [linecache module](http://www.doughellmann.com/PyMOTW/linecache/index.html)
* If you need to create a temporary file, use the [tempfile module](http://www.doughellmann.com/PyMOTW/tempfile/index.html#module-tempfile)
* To work with compressed files (.tar, .bz2, .gz, .zip), use any of the [data compression modules](http://www.doughellmann.com/PyMOTW/compression.html)

---
# Reading CSV files

* To read CSV files use the [csv module](http://docs.python.org/library/csv.html). Here are some examples [PyMOTW csv](http://www.doughellmann.com/PyMOTW/csv/index.html#module-csv)
* The **csv** module gets a bad rap for being inefficient, different modules implement their own version of csv parsing like **numpy.loadtxt**
* If you need to read a csv file in a context of a library, check first if the library includes a csv reader
* **pandas** is going a very efficient csv parser. This blog post explains everything [A new high performance, memory-efficient file parser engine for pandas](http://wesmckinney.com/blog/?p=543)

---
# Serialization in Python

* Process of converting a data structure or object into a format that can be stored and materialized at a later time
* Deserialization is the opposite process (materializing)
* In therory, serialization allows to create an identical clone of the original object
* For objects with references, i.e. shallow copies, the process of serialization is not straighforward. 

---
# Serialization in Python

.notes: [http://www.doughellmann.com/PyMOTW/pickle/index.html](http://www.doughellmann.com/PyMOTW/pickle/index.html)

* Python uses the **pickle** module to serialize Python objects into a stream of bytes suitable for storing in a file, transferring across a network, or placing in a database. 
* The process is called pickling, serializing, marshalling, or flattening. 
* The resulting byte stream can also be converted back into a series of Python objects using an unpickling process.
* **cPickle** is another module with the same interface much faster than **pickle** but you can't subclass it
* If subclassing is not important, use **cPickle**
* Use **pickle.dump**, **pickle.dumps**, **cPickle.Pickler** to serialize objects
* Use **pickle.dump**, **pickle.dumps**, **cPickle.Unpickler** to materialize objects

---
# Example

    !python
    try:
        import cPickle as pickle
    except:
        import pickle
    import pprint

    data = [ { 'a':'A', 'b':2, 'c':3.0 } ]
    print 'DATA:',
    pprint.pprint(data)

    data_string = pickle.dumps(data)
    print 'PICKLE:', data_string

    data2 = pickle.loads(data1_string)
    print 'AFTER:',
    pprint.pprint(data2)

---
# Example

    !python
    PICKLE_FILE = "pickle_file.dat"
    out_s = open(PICKLE_FILE, 'wb')
    try:
        # Write to the stream
        print 'writing to a file...'
        pickle.dump(data1, out_s)
    finally:
        out_s.close()
        
    in_s = open(PICKLE_FILE, 'rb')
    try:
        try:
            print 'reading from file...'
            data3 = pickle.load(in_s)
        except EOFError:
            print 'Oops, error reading from file.'
        else:
            pprint.pprint(data3)
            print 'SAME?:', (data1 is data3)
            print 'EQUAL?:', (data1 == data3)
    finally:
        in_s.close()
    
---
# Reading Excel (if you have to)

* There are currently two popular modules to read excel files
* [xlrd](https://secure.simplistix.co.uk/svn/xlrd/trunk/xlrd/doc/xlrd.html) lets you read Excel XLS files 97-2003 formats.
* [openpyxl](http://packages.python.org/openpyxl/) lets you read Excel XLSX files 2007 up formats.









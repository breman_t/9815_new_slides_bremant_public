# MFE 9815<p>Software Engineering in Finance<br/>Breman Thuraisingham _Morgan Stanley_<br/>Alain Ledon _JP Morgan Chase_</p>

---

# Today We'll Cover
* Open Source
* The Unix Shell
* Using Bash
* Common Unix Tools 
* Unix Shell Scripting

---

# Open Source*

.notes: [Learn more about free software.](http://www.gnu.org/philosophy/free-sw.html)

Complementing the tool-oriented approach are the open source and liberal licensing schemes of most Unix distributions. 

![we-are-open](open-small.png)

* The freedom to run the program, for any purpose.
* The freedom to study how the program works, and change it so it does your computing as you wish.
* The freedom to redistribute copies so you can help others.
* The freedom to distribute copies of your modified versions to others. 

<font color="green">_By doing this you can give the whole community a chance to benefit from your changes and vice versa. Access to the source code is a precondition for learning._</font>


---

# You Want Me to Share My Code?*

.notes: [On the cruelty of really teaching computer science](http://www.cs.utexas.edu/~EWD/ewd10xx/EWD1036.PDF)

![tough-baby](tough-baby-sm.png)

* You gain little by building your own commodity services.
* Software development is _extremely difficult_. 
* Allowing others to review your code will make you a better developer. 
* The cost of coordination and collaboration has dropped dramatically.

---

# Beware Enterprise Software*

.notes: [Microsoft Team Foundation Server](http://www.derekhammer.com/2011/09/11/tfs-is-destroying-your-development-capacity.html)

<font color="maroon"><h1>Avoid outdated distribution models that burden your processes.</h1></font>

![fat-cat](fat-cat-sm.png)

_<h1>Essence</h1>_

Details related to the problem you are trying to solve. 

_<h1>Ceremony</h1>_

All of the extraneous details unrelated to your problem that you must still address to make progress. These include licensing, auditing, and activation. May also include sales meetings, contract negotiations, project managers, vendor lock-in, poor documentation, and changes in corporate direction.

<font color="green"><h1>To get things done, keep the ratio of ceremony to essence low.</h1></font>

---

# The Unix Shell*

.notes: [A history of Unix](http://www.unix.org/what_is_unix/history_timeline.html) and [Classic Shell Scripting](http://oreilly.com/catalog/9780596005955)

Unix was developed within a research-oriented environment; there was no commercial pressure to produce a specific product.

![unix-logo](linux-logo.png)

* The system was developed by its users. They used their own products everday.
* The researchers were free to experiment and to change programs as needed.
* The researchers were highly educated computer scientists designing tools for themselves and colleagues that were also computer scientists.
* The researchers aimed for efficiency and elegance.
 
This approach, originally part of Bell Labs, is now part of the general Unix ecosystem. 

---

# Development Stack

* Linux
* Python
* MySQL
* C++

In the last class, we created a virtual environment for our development. Today, we'll explore the low-ceremony beauty of the Unix Shell.

The core principle that we'll build upon is that _"the power of a system comes more from the relationships among programs than from the the programs themselves"_.

No graphical user interface is as lean or as powerful as the classic Unix shell. 

Shell programs are tools that are _specific in function_ but usable for _different purposes_.

---

# Using Bash*

.notes: [Unix Power Tools](http://www.amazon.com/Power-Tools-Third-Shelley-Powers/dp/0596003307)

![shell-env](shell-environment.png)

To be general purpose tools, our programs must be _data independent_:

* Within limits, the output of any program should be usable as the input to another.
* All of the information needed by a program should be either contained in the data stream passed to it or specified on the command line. _A program should not prompt for input or do uncessary formatting of output._ 
* If no arguments are given, a program should read the standard input (usually the terminal keyboard) and write to the standard output (usually the terminal screen).

---

# User help one command away

The Unix shell comes with built-in help, the `man` command. You can use _man <command>_ to get the manual of the given command:

For example:

    !bash
    $ man ls
    
It will give you all the help associated with the list directory _ls_ command.

---

# Simple commands

* _ls_ = List all the artifacts in the current directory
* _cp_ = Copy files
* _mv_ = Move/Rename files
* _rm_ = Remove/Delete files (non recoverable)
* _pwd_ = Gives you current working directory
* _echo_ = Prints a string to the terminal
* _cd_ = Change directories
* _mkdir_ = Make a directory
* _apt-get_ = Install software
* _apt-cache_ = Search for software


---

# A Tour of Common Unix Tools

* `grep`
* `find`
* `cut`
* `sort`
* `wc`
* `uniq`
* `comm`
* `join`
* `sed`
* `(g)awk`
* `curl`
* ...
---
 
# Using `grep`*

.notes: [Regular expression reference](http://www.regular-expressions.info/reference.html)

`grep` is an acronym for _global regular expression print_. The main function of `grep` is to search text for strings that satisfy a _regular expression_ (a pattern of text) and print lines that match. 

For example, let's find all the processes on our machine that belong to our web server.

    !bash
    $ ps -ef
    UID        PID  PPID  C STIME TTY          TIME CMD
    root         1     0  0 Aug20 ?        00:00:03 /sbin/init
    root         2     0  0 Aug20 ?        00:00:00 [kthreadd]
    root         3     2  0 Aug20 ?        00:00:00 [migration/0]
    root         4     2  0 Aug20 ?        00:00:23 [ksoftirqd/0]
    root         5     2  0 Aug20 ?        00:00:00 [watchdog/0]
    
There are hundreds of lines! We can select the ones we want with `grep`.

    !bash
    $ ps -ef | grep apache
    root     21457     1  0 Sep02 ?        00:00:08 /usr/sbin/apache2 -k start
    www-data 22507 21457  0 07:43 ?        00:00:00 /usr/sbin/apache2 -k start
    www-data 22508 21457  0 07:43 ?        00:00:00 /usr/sbin/apache2 -k start
    
Now we have just the processes that are related to our web server.
---

# Using `grep` (cont'd)

Regular expressions are extremely powerful and worth learning. 

    !bash
    # search scripts in current folder for "mvn"
    $ grep "mvn" *.sh
    bb-alfresco-run.sh:MAVEN_OPTS="-Xmx512m -XX:MaxPermSize=128m" mvn install -Prun
    bb-jetty-run.sh:mvn "${FC_JAVA_OPTS}" -Druntime-context=${runtime} -Dlogkit.dir=/tmp -Djetty.port="${port}" $CLEAN jetty:run 
    bb-make-assembly-for-space.sh:# make the assembly by mvn;
    bb-make-assembly-for-space.sh:mvn assembly:assembly -DdescriptorId=jar-with-dependencies -Djini-scope=provided -Dmaven.test.skip=true 
    ...

With a single switch, you can ask `grep` to include the line number of the match:

    !bash
    $ grep -n mvn *
    bb-alfresco-run.sh:3:MAVEN_OPTS="-Xmx512m -XX:MaxPermSize=128m" mvn install -Prun
    bb-jetty-run.sh:25:mvn "${FC_JAVA_OPTS}" -Druntime-context=${runtime} -Dlogkit.dir=/tmp -Djetty.port="${port}" $CLEAN jetty:run 
    bb-make-assembly-for-space.sh:3:# make the assembly by mvn;
    bb-make-assembly-for-space.sh:4:mvn assembly:assembly -DdescriptorId=jar-with-dependencies -Djini-scope=provided -Dmaven.test.skip=true 

Or even count the number of occurrences:

    !bash
    $ grep -c "mvn" *.sh
    bb-alfresco-backup.sh:0
    bb-alfresco-run.sh:1
    bb-alfresco.sh:0
    bb-bsym-fetch.sh:0
    bb-create-alfresco.sh:0

---

#Using `find`

Let's find all shell scripts in the current folder and subfolders:

    !bash
    find . -name "*.sh"

Now let's find all shell scripts do something with the epm tool:

    !bash
    find . -name "*.sh" -exec grep -l "epm" {} \;

By using the `exec` option with `find`, we can process every match in line.

How about files (not folders) that were modified seven or fewer days ago?

    !bash
    $ find . -type f -mtime -7

`find` has scores of options that allow you to fine tune your matches with extreme precision.

---

# Using `cut`

`cut` enables you to select a list of columns (fields) from one or more files.

You must specify `-c` to cut by column or `-f` to cut by fields. Fields may be separated by tabs, commas, or just about any other character. You specify which with the `-d` option.

Find who is logged in, but list only login names:

    !bash
    $ $ who | cut -d" " -f1
    nkabir
    nkabir

Extract usernames and real names from `/etc/passwd`:

    !bash
    $ cut -d: -f1,5 /etc/passwd
    root:root
    daemon:daemon
    bin:bin
    sys:sys

---

# Using `sort`*

.notes: [Download phone-list.txt](http://package.fimero.org/baruch/9815/phone-list.txt)

Sort lines of text with options that give you fine-grained control over field separators and column priority.

    !bash
    $ cat phone-list.txt 
    Robert M Johnson    344−0909
    Lyndon B Johnson    933−1423
    Samuel H Johnson    754−2542

Calling `sort` without any options sorts on the first field (leftmost), then on the second, and so on.

    !bash
    $ sort phone-list.txt 
    Lyndon B Johnson    933−1423
    Robert M Johnson    344−0909
    Samuel H Johnson    754−2542

Like most Unix tools, `sort` gives us a great deal of control to modify its default behavior. We want to sort on field 2, continue on field 0 and 1 and not sort on field 3 (the phone number):

    !bash
    $ sort +2 -3 +0 -2 phone-list.txt 
    Robert M Johnson    344−0909
    Samuel H Johnson    754−2542
    Lyndon B Johnson    933−1423

---

# Using `wc`

`wc` counts the number of lines, words and characters in a file. For example,

    !bash
    $ wc phone-list.txt 
     3 12 93 phone-list.txt

There are 3 lines, 12 words and 93 characters in our file. You can restrict what is counted with command line options: 

* `-l` for lines
* `-w` for words
* `-c` for characters.

---

# Using `uniq`

`uniq` is used to remove duplicate _adjacent_ lines. How do we get lines to be adjacent? We use `sort`. This demonstrates the pipeline approach common in Unix shell programming.

    !bash
    $ cat phone-list.txt 
    Samuel H Johnson    754−2542
    Robert M Johnson    344−0909
    Samuel H Johnson    754−2542
    Lyndon B Johnson    933−1423
    Samuel H Johnson    754−2542

Samuel appears three times! Let's fix that.

    !bash
    $ cat phone-list.txt | sort | uniq
    Lyndon B Johnson    933−1423
    Robert M Johnson    344−0909
    Samuel H Johnson    754−2542

And, of course, `uniq` has several options to fine tune its behavior.

---

# Using `comm`

`comm` enables you to compare the contents of two lists and determine which entries appear uniquely in one or the other. 

Let's say we have two files (data1.txt and data2.txt) that contain security ids (we'll just use single characters and assume both files are sorted). 

Which securities are only in the second data set?

    !bash
    $ comm -13 data1.txt data2.txt 

    c
    d
    t

Which securities are only in the first data set?

    !bash
    $ comm -23 data1.txt data2.txt 
    i
    q
    z

Which securities are in both data sets?

    !bash
    $ comm -12 data1.txt data2.txt 
    a
    b
    e

---

# Using `join`

You've probably encountered the notion of a _join_ if you've ever worked with a relational database (SQL).

We have two files `securities.csv` and `positions.csv` that contain security definitions and our positions respectively.

    !bash
    $ cat securities.txt 
    1,IBM,USD
    2,AAPL,USD
    3,XO,USD

    $ cat positions.txt 
    1,1000
    2,3252
    3,5220

`join` enables us to combine the two files keyed on their ids.

    !bash
    $ join -t, securities.txt positions.txt 
    1,IBM,USD,1000
    2,AAPL,USD,3252
    3,XO,USD,5220

---

# Using `sed`*

.notes: [Sed & Awk](http://shop.oreilly.com/product/9781565922259.do)

Note that `sed` and `(g)awk` (covered next) are extremely versatile tools that cannot be covered in detail here. It will serve you well to familiarize yourself with the scope of their capabilities.

Think of `sed` as a programmable editor for text files. There are two details to keep in mind:

* By default, `sed` doesn't change the file it edits. As a _stream editor_, it takes a stream of data, transforms it inline, and passes to standard out. This allows you to keep your original data untouched and process your changes in a pipeline.
* `sed` commands are implicitly global but you can restrict them to operate on subsections of your input files.

A typical use of `sed` is 

    !bash
    % somecommand | sed 's/oldval/newval/' | othercommand

This takes the output of `somecommand` and replaces all occurrences of _oldval_ with _newval_. The output is then processed by `othercommand`.

You can combine several `sed` transformation in a single file and call them with `-f`

    !bash
    sed -f mysedfile mytargetfile

---

# Using `(g)awk`*

.notes: [Effective Awk Programming](http://shop.oreilly.com/product/9780596000707.do)

`(g)awk` is extremely powerful and is Turing-complete. It is the most complex Unix tool we'll use before moving on to Python. `(g)awk` is invoked in two ways:

    !bash
    awk [options] 'script' [var=value] [file(s)]

Or, if you've saved your `(g)awk` script in a file:

    !bash
    awk [options] -f scriptfile [var=value] [file(s)]

From our earlier example, let's say we wanted a quick reference for our phone contacts:

    !bash
    $ cat phone-list.txt | sort | uniq | gawk '{ print $4 " " $1 }'
    933−1423 Lyndon
    344−0909 Robert
    754−2542 Samuel

We've taken our original phone list (with duplicates), sorted it, removed duplicates and re-arranged the columns.

Learn `(g)awk`. It is extremely powerful and will save you a lot of time when you need to manipulate and reformat data.

---

# Using `curl`

Up to this point, everything we've been doing involves data that resides on our computer. We live in a networked world and need a quick and easy way to access external data. 

`curl` is a command-line tool that allows you to interact with HTTP resources. It's extremely flexible and, like every other tool we've covered, well worth learning. You can use it to automate downloads, logins, and just about anything else you can do with your browser.

For example, Yahoo makes equity data available via

    !bash
    http://finance.yahoo.com/d/quotes.csv?s= a BUNCH of STOCK SYMBOLS separated by "+" &f=a bunch of special tags

Using `curl`, we can download the data as:

    !bash
    $ curl -L -o mydata.txt 'http://finance.yahoo.com/d/quotes.csv?s=XOM+BBDb.TO+JNJ+MSFT&f=snd1l1yr'
    "XOM","Exxon Mobil Corpo","9/12/2011",70.59,2.56,9.36
    "BBD-B.TO","BOMBARDIER INC., ","9/12/2011",4.25,1.71,9.15
    "JNJ","Johnson & Johnson","9/12/2011",62.6675,3.49,15.22
    "MSFT","Microsoft Corpora","9/12/2011",25.4978,2.49,9.57

This data should now be easy for you to manipulate using the tools we've covered.

---

# Change permissions 'chmod' *

.notes: [https://kb.iu.edu/d/abdb](https://kb.iu.edu/d/abdb)

In Linux permissions are determined by Access control. You can restrict reading/writing or execution persimission to a file for a user or a group or users not in the group or all. 
The command to do this is `chmod`.

Examples:

    !bash
    chmod +x script.sh
    
makes script.sh *executable*. It assumes all.

    !bash
    chmod a+w script.sh
    
gives every user *write* permission to script.sh.

---

# Shell Scripting

You can combine multiple shell commands in a script to programmatically execute them in one pass. The file with all the commands is usually called shell script. By convention we put extension _.sh/.bsh/.ksh_ 

The first line must be the shell you are using

    !bash
    #!/bin/bash
    
    VAR1="Alain" 
    VAR2="Ledon"
    
    echo "this is a script" $VAR1 ${VAR2}
    pwd
    ls -1

    CURR_DIR=$(pwd)
    echo "This is the current directory = " ${CURR_DIR}

---

# Variables

---

# Command Execution and Assignment

---

# For loops

---

# Conditionals

---
# References 

* [LinuxCommand.org](http://linuxcommand.org/)
* [Linux Shell Scripting Tutorial](http://www.freeos.com/guides/lsst/)
* [BASH Programming](http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html)
* [Bash Guide for Beginners](http://www.tldp.org/LDP/Bash-Beginners-Guide/html/)
* [Unix Power Tools](http://goo.gl/eLhvo6)
* [Linux Shell Scripting Cookbook](http://amzn.com/1782162747)
* [Data Science at the Command Line: Facing the Future with Time-Tested Tools](http://amzn.com/1491947853)

---

# Quandl

Quandl is a company that makes available multiple data sets over the web for free (or paid). They also provide an [REST API](https://www.quandl.com/tools/api) to access their data.

You will need an account with Quandl to obtain an API KEY. Registered users have a limit of 2,000 calls per 10 minutes, and a limit of 50,000 calls per day.

Example datasets:

* [Fannie 30Y Rate PMMS](https://www.quandl.com/data/FMAC/FIX30YR-Primary-Mortgage-Market-Survey-30-Year-Fixed-Rate)
* [AAPL End-of-Day Prices](https://www.quandl.com/data/WIKI/AAPL-Apple-Inc-AAPL)
* [JPM End-of-Day Prices](https://www.quandl.com/data/WIKI/JPM-JP-Morgan-Chase-JPM-Prices-Dividends-Splits-and-Trading-Volume)
* [JNJ End-of-Day Prices](https://www.quandl.com/data/WIKI/JNJ-Johnson-Johnson-JNJ-Prices-Dividends-Splits-and-Trading-Volume)
* [AAPL EOD CSV output](https://www.quandl.com/api/v3/datasets/WIKI/AAPL.csv)


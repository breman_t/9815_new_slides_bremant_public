# MFE 9815<p>Software Engineering in Finance<br/>Class 4 - Python II<br/>Norman Kabir _Autonomy Capital Research_<br/>Alain Ledon _Ally Financial_</p>

---
# Today We'll Cover

* Dictionaries
* More about functions
* Python built-in functions
* Code Organization (Packages and Modules)
* Object Oriented Programming

---
# Dictionaries (I)*

.notes: [Mapping Types - dict](http://docs.python.org/library/stdtypes.html?highlight=dict#dict)

* **Dictionaries** map _hashable_ values to arbitrary objects (_keys_). It defines one-to-one relationships between keys and values.
* An object is hashable if it has a hash value which never changes during its lifetime (it needs a __hash__() method), and can be compared to other objects (it needs an __eq__() or __cmp__() method). Hashable objects which compare equal must have the same hash value.
* **Dictionaries** are called _hash_, _map_ or _associative arrays_ in other languages.
* A dictionary's keys are _almost_ arbitrary values.
* Keys must be unique.

---
# Dictionaries (II)

Creating dictionaries and getting values

    !python
    In [1]: webster = {}
    In [2]: webster = {'one': 1, 'two': 2}
    In [3]: webster['three'] = 3
    In [4]: webster['four'] = range(3)
    In [5]: webster
    Out[5]: {'four': [0, 1, 2], 'one': 1, 'three': 3, 'two': 2}
    In [6]: dict(one=1, two=2)
    Out[6]: {'one': 1, 'two': 2}
    In [7]: dict({'one': 1, 'two': 2})
    Out[7]: {'one': 1, 'two': 2}
    In [8]: dict(zip(('one', 'two'), (1, 2)))
    Out[8]: {'one': 1, 'two': 2}
    In [9]: dict([['two', 2], ['one', 1]])
    Out[9]: {'one': 1, 'two': 2}
    In [10]: webster['one']
    Out[10]: 1
    In [11]: webster.get('one')
    Out[11]: 1
    In [12]: del webster['two']
    In [13]: webster
    Out[13]: {'four': [0, 1, 2], 'one': 1, 'three': 3}
    
The order of insertion is not preserved.

---
# Dictionaries (III)

Traversing dictionary keys/values/items

    !python
    In [13]: for i in webster:
        ...
    In [14]: for i in webster.keys():
        ...
    In [15]: for i in webster.iterkeys():
        ...
    In [16]: for i in webster.values():
        ...
    In [17]: for i in webster.itervalues():
        ...
    In [18]: for i in webster.items():
        ...
    In [19]: for i in webster.iteritems():
        ...
        
---
# Dictionaries

![Merriam-Webster Dictionaries](mw2.jpeg)

 * [http://docs.python.org/tutorial/datastructures.html#dictionaries](http://docs.python.org/tutorial/datastructures.html#dictionaries)
 * [http://docs.python.org/library/stdtypes.html?highlight=dict#dict](http://docs.python.org/library/stdtypes.html?highlight=dict#dict)
 
---
# Functions (cont.)

* Functions are first class citizens. 
* They are like any other variable

From [Class 2](class-02-python-2012.html)

    !python
    In [17]: def square(val):
       ....:     return val * val
       ....:

    In [18]: xx = square

    In [19]: xx(2)
    Out[19]: 4
    
---
# Functions (cont.)

You can use functions as parameters.

    !python
    In [20]: yy = lambda val : val * val

    In [21]: yy(4)
    Out[21]: 16
    
    In [26]: map(yy, [1, 2, 3, 4, 5])
    Out[26]: [1, 4, 9, 16, 25]

---
# Functions (cont.)

    !python
    In [31]: def summult2(a, b):
       ....:     return a + 2, b * 2
       ....:

    In [32]: summult2(3, 4)
    Out[32]: (5, 8)

    In [33]: x, y = summult2(3, 4)

    In [34]: x
    Out[34]: 5

    In [35]: y
    Out[35]: 8

---
# Functions (cont.)

You can pass tuples or listst into functions and apply variable expansion using ** * **

    !python
    In [27]: def sum2(a, b):
       ....:     return a + b
       ....:

    In [28]: x = (3, 4)

    In [30]: sum2(*x)
    Out[30]: 7

    In [31]: pp = [5, 3]

    In [32]: sum2(*pp)
    Out[32]: 8

---
# Multiple Parameters

* Functions can accept un-named multiple parameters by using the positional expansion operator ** * ** .
* A parameter name that begins with * gathers arguments into a tuple as they are passed into the functions.
* Effectively, the function can expect mutiple parameters and act accordingly.

Here is an example:

    !python
    In [10]: def margs(*args):
    ....:     for p in args:
    ....:         print p
    ....:         
    In [11]: margs(5, 4, 3)
    5
    4
    3

    In [12]: margs(5, 4, "alain") 
    5
    4
    alain

---
# Keyword Parameters

* Functions accept keyword arguments that match the variable name (check [Class 2](class-02-python-2012.html))
* Functions can accept multiple keyword parameters as well by using ** operator.
* A parameter name that begins with ** will gather arguments into a dictionary as they are passed into the functions.
* Keyword parameters must follow positional parameters.

Here is an example:

    !python
    In [63]: def mkwargs(first, **kwargs):
       ....:     for k,v in kwargs.items():
       ....:         print "{0} | {1} = {2}".format(first, k, v)
       ....:
       ....:
       
    In [64]: mkwargs("parameters", a=1,b=2,c=3)
    parameters | a = 1
    parameters | c = 3
    parameters | b = 2

---
# More about multiple parameters

You can combine un-named and named paramters in your function for maximum flexibility.

    !python
    In [73]: def argorgy(first, *args, **kwargs):
        ....:     print "This parameter must be passed {0}".format(first)
        ....:     for i, v in enumerate(args):
        ....:         print "un-named param {0} -> {1}".format(i, v)
        ....:
        ....:     for k, v in kwargs.items():
        ....:         print "named param {0} = {1}".format(k, v)
        ....:

    In [75]: argorgy("alain", 1, 2, 3, four=4, five=5, six=6)
    This parameter must be passed alain
    un-named param 0 -> 1
    un-named param 1 -> 2
    un-named param 2 -> 3
    named param four = 4
    named param six = 6
    named param five = 5

---
# Function Overloading

* Python doesn't really support overloading as in C++ (same function name different signature)
* If you want to overload a function, use multiple parameters

---
# Python built-in functions*

.notes: [Built-in functions](http://docs.python.org/library/functions.html)
    
![Python built-in functions](python_builtin.png)

## Don't re-invent the wheel. Use built-in functions whenever possible 

---
# Python built-in functions (cont.)*

Here are some examples:

    !python
    In [13]: max(range(10))
    Out[13]: 9

    In [14]: min(range(10))
    Out[14]: 1

    In [17]: all(range(10))
    Out[17]: False

    In [18]: any(range(10))
    Out[18]: True

    In [19]: sum(range(1,101))
    Out[19]: 5050

    In [20]: reduce(lambda x,y: x * y, range(1,10), 1)
    Out[20]: 362880

    In [21]: map(lambda x: x * x, range(1,10))
    Out[21]: [1, 4, 9, 16, 25, 36, 49, 64, 81]

    In [26]: filter(lambda x: x % 2 != 0, range(10))
    Out[26]: [1, 3, 5, 7, 9]

---
# Code Organization

* Python scripts are organized in _packages_ and _modules_
* All python script (*.py) is considered a _module_
* A directory containing python scripts could become a _package_
* Use the keyword **import** to get access to module(s) and module(s) inside package(s)

---
# Modules and Packages

By definition all python scripts are _modules_. The *import* keyword provides module access; it imports the module as a whole.

    !python
    import module [, module]*
    import [package.]* module [, [package.]* module]*
    import [package.]* module as name [, [package.]* module as name]*
    from [package.]* module import module[, module]*
    
* You can access module content by qualification (e.g., _module.attribute_). 
* The _as_ clause assigns a variable _name_ to the imported module. It can be seen as an alias.
* Import operations compile file's source to byte-code if needed (and save it as _.pyc_). Then execute the compiled code from top to bottom.
* Note that you can't import a package, only a module(s) from a package.
* You can use the keyword **from** to import specific functions, attributes and classes directly into the local namespace
* _from mymodule import *_ will import everything from _mymodule_ into the local namespace (this is discouraged).

---
# Packages

* A _package_ is a directory containing python _modules_.
* It must have a (possibly empty) ** __init__.py ** script that serves as directory level's module namespace.
* The ** __init__.py ** script will be executed on the first import through the directory and all names assigned in it will become attributes of the directory's module object
* An import of the form **import dir1.dir2.pyscript** will load the module file at directory path _dir1/dir2/pyscript.py_

---
# Search Path for _import_

* **import** uses **sys.path** as module import search path
* **sys.path** is a directory list initialized from the program's top-level directory, **PYTHONPATH** settings, **.pth** path file contents and python defaults
* **PYTHONPATH** is environment variable that is added to the search path. It can be set/modified before you run your Python program
* **.pth** files are simple files with a directory per line. Those directories will be added to **sys.path**

---
# Object Oriented Programming

* Python support object oriented programming by using the keyword **class**.
* Access modifiers (public, private, protected) are not enforced by the language. Remember, we are all adults.
* Python supports multiple inheritance
* Full polymorphism support 
* Class' members are declared inside the **class** scope
* Instance members are accessed using keyword **self**.
* Members not accessed using **self** are Class' members (shared among all Class' instances). 

---
# Classes

There are two ways to declare classes in Python 2.x. We are going to use new-style classes:

    !python
    # new-style classes
    class Shape(object):
        common = "EVERYBODY"

        def __init__(self, name):
            self.name = name

        def whoami(self):
            print "I'm a shape: {0}".format(self.name)

        def anything_else(self):
            print "nothing"
 
* Class _Shape_ inherits from class _object_. This is the parent class of all new-style classes.
* It has two methods _whoami_ and _anything_else_
* The special method __init__ is the constructor.
* Notice all methods have as first parameter, **self**.

---
# Classes (cont.)

How to use it?

    !python
    In [10]: xx = Shape("Red")

    In [11]: xx.whoami()
    I'm a shape: Red

    In [12]: xx.common
    Out[12]: 'EVERYBODY'

    In [13]: xx.anything_else()
    nothing

    In [14]: xx.name
    Out[14]: 'Red'


* Since all members are public, you can access any of them freely.
* PEP-8 recommends using **__double_leading_underscore** when naming a class attribute (private attributes). It invokes name mangling, i.e. **Foo.__a** becomes **Foo._Foo__a**.

---
# Inheritance and Polymorphism

* Python supports single and multiple inheritance. 
* You only need to specify in the class declaration inside parenthesis who the parent class(es) is(are).
* All the ancestors' members (including the constructor) can be accessed using the built-in function **super(...)**
* Multiple inheritance follows the [C3 Method Resolution Order ](http://www.python.org/download/releases/2.3/mro/)


---
# Inheritance and Polymorphism (cont.)

Example:

    !python
    class Circle(Shape):

        def __init__(self, name, radius):
            super(Circle, self).__init__(name)
            self.radius = radius

        def anything_else(self):
            print "I have area = {0}".format(3.14159 * self.radius)

    def playing(something):
        something.whoami()
        something.anything_else()

---
# Inheritance and Polymorphism (cont.)

Here are the results:

    !python
    In [28]: xx = Shape("Red")

    In [29]: cc = Circle("Blue", 4.)

    In [30]: playing(xx)
    I'm a shape: Red
    nothing

    In [31]: playing(cc)
    I'm a shape: Blue
    I have area = 12.56636

---
# Duck Typing

* Python's object system exhibit a behavior called _duck typing_. 
* "If walks like a duck, and quacks like a duck, it must be a duck."
* This is how polymorphism is really implemented. 

Following the previous example:

    !python
    class Howard(object):

        def whoami(self):
            print "I'm Howard"

        def anything_else(self):
            print "Don't know Howard? Howard the Duck"

---
# Duck Typing (cont.)

Here are the results:

    !python
    In [64]: xx = Shape("red")

    In [65]: cc = Circle("blue", 4)

    In [66]: dd = Howard()

    In [67]: playing(xx)
    I'm a shape: red
    nothing

    In [68]: playing(cc)
    I'm a shape: blue
    I have area = 12.56636

    In [69]: playing(dd)
    I'm Howard
    Don't know Howard? Howard the Duck

The last class _Howard_ is not part of the inheritance tree but it still works

---
# Howard the Duck

![Howard the Duck](Howard_the_Duck.jpg)

[Howard the Duck on Wikipedia](http://en.wikipedia.org/wiki/Howard_the_Duck_(film))

--- 
# Special Method Names*

.notes: [Python Special Method Names](http://docs.python.org/reference/datamodel.html#special-method-names)

* A class can implement certain operations that are invoked by special syntax (such as arithmetic operations or subscripting and slicing) by defining methods with special names. 
* This is Python's approach to operator overloading. 
* When implementing a class that emulates any built-in type, it is important that the emulation only be implemented to the degree that it makes sense for the object being modelled. 
* Notice, operator overloading can obscure your code. Make sure it makes sense to implement them the special methods.

---
# Special Method Names (cont.)

* **__repr__(self)** for _repr()_ and _`...`_ conversions (used by debuggers).
* **__str__(self)**  for _str()_ and _print_ statement.
* **__lt__(self, other)** Called for self < other comparisons.  
* **__gt__(self, other)** Called for self > other comparisons. 
* **__eq__(self, other)** Called for self == other comparisons. 
* **__ne__(self, other)** Called for self != other (and self <> other) comparisons. 
* **__call__(self, *args, **kwargs)** Called when an instance is called as function: obj(...) 
* and much more...

[Python Documentation Special Method Names](http://docs.python.org/reference/datamodel.html#special-method-names)

[Python Special Method Names](http://rgruet.free.fr/PQR26/PQR2.6.html#SpecialMethods)

---
# Special Method Example

    !python
    class BaseBallPlayer(object):

        def __init__(self, name, knick, rings):
            self.name = name
            self.knick = knick
            self.rings = rings
            
        def __str__(self):
            return "I'm {0}".format(self.knick)

        def __repr__(self):
            return "I'm a baseball player named {0}".format(self.name)
        
        def __gt__(self, other):
            return self.rings > other.rings
        
        def __lt__(self, other):
            return self.rings < other.rings
        
        def __eq__(self, other):
            return self.rings == other.rings

        def __ne__(self, other):
            return not self.__eq__(other)
        
        def __ge__(self, other):
            return self.__gt__(other) or self.__eq__(other)  

        def __len__(self):
            return self.rings 
    
---
# Special Method Results

    !python
    In [51]: d = BaseBallPlayer("DJ", "The Captain", 5)

    In [52]: a = BaseBallPlayer("ARod", "Pretty Boy", 1)

    In [53]: y = BaseBallPlayer("Berra", "Yogi", 13)

    In [54]: a
    Out[54]: I'm a baseball player named ARod

    In [55]: d
    Out[55]: I'm a baseball player named DJ

    In [56]: y
    Out[56]: I'm a baseball player named Berra

    In [57]: a > y
    Out[57]: False

    In [58]: print a
    I'm Pretty Boy

    In [59]: a
    Out[59]: I'm a baseball player named ARod

    In [60]: len(a)
    Out[60]: 1

    In [61]: len(y)
    Out[61]: 13

    In [62]: print y
    I'm Yogi
 
--- 
# Yogi Berra

![Yogi Berra](berra13.jpg)

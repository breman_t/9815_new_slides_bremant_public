# MFE 9815<p>Software Engineering in Finance<br/>10<br/>Norman Kabir _Autonomy Capital Research_<br/>Alain Ledon _Ally Financial_</p>

---

.qr: 450|http://package.fimero.org/baruch/9815/class-10.pdf

---

# Today We'll Cover

* Creating Objects
* String Representations for REPL
* Measure 
* SerialDateOperator

---

# Constructors and KITs

Do not expose constructors in your API. A Class is an _implementation detail_.

## Think "KIT"

![stamp-press](stamp-press-sm.jpg)

* <strong>K</strong>it - an entity responsible for instantiating an object
* <strong>I</strong>nterface - that is returned by your Kit
* <strong>T</strong>ype - actual implementation (not exposed)

<div class="big important center">
Aim for low coupling, high cohesion among your classes.
</div>

---

# SerialDate

Our `SerialDate` class represents number of days since January 1, 1900.

## Why use `SerialDate`?

* We can subtract two SerialDates to produce the actual number of _solar_ days (nycthemeron)
* We can add an integer to a SerialDate to produce a new SerialDate

---

# Making SerialDates

    !python
    from fimero.time import SerialDate
    d1 = SerialDate.make(20110202)

We've created a `make` method for `SerialDate`.

    !python
    def make(date_value):
        value_type = type(date_value)
        # Note that strings can be ascii or unicode
        if isinstance(date_value, types.StringTypes):
            value_type = types.StringType
        serialdaynumber = date_value
        if value_type is not float:    
            converter = CONVERTERS.get(value_type)
            serialdaynumber = converter(date_value)
        return SerialDate(serialdaynumber)

* This logic is not relevant to a `SerialDate` but rather belongs in a `SerialDate` _kit_.
* Keep your classes as focused as possible.
* In Python, you can create module functions to instantiate classes.
* Clients of your API do not need to know which class is returned so you are free to change it.

---

# Printing SerialDate

## Unless you specify a string representation, your class will print as an object address.

    !python
    print d1
    <fimero.time.SerialDate.SerialDate object at 0x1ec6410>

<div class="big important center">
This is useless.
</div>

Python defines two methods used to convert an object into a string:

* `__repr__` goal is to be unamiguous
* `__str__` goal is to be human-readable
* Containers (lists, dictionaries) use contained objects' `__repr__`

Note that if `__repr__` is defined but `__str__` is not, `__repr__` will be used when `str()` is called.

---

# Implement `repr` and `str`

## `repr` should be unambiguous 

    !python
    def __repr__(self):
        """Return a string representation of this class.
        """
        return self.__class__.__name__ + "(" + repr(self.serial_day_number) + ")"

    d1 = SerialDate.make('20110202')
    SerialDate(40576.0)


Much better! But although `SerialDate(40576.0)` represents the object's state, it's still not human-friendly.

## `str` should be human-friendly

    !python

    def __str__(self):
        """Return a human-friendly string for this class.
        """
        return repr(self) + " " + str(self.as_datetime()) 

We've enhanced `repr` to return a Gregorian representation of our date.

    !python
    print d1
    SerialDate(40576.0) 2011-02-02 00:00:00


---

# Measure SerialDates Intervals

## There are numerous financial day count conventions.
In effect, this means that the distance between two points on our time line depends on the convention we use!

* Actual/Actual
* Actual/360
* 30/360

## We need an API

Generally, we need to

* Count number of days in an interval
* Calculate a _day count factor_ (often year fraction)

---

# Measure API

    !python
    # We'll need two methods
    count_days(beg, end) # returns number of days
    dcf(beg, end) # returns 'day count factor'

## Base Class

    !python
    class Measure(object):
        name = None

        def count_days(self, beg, end):
            return end.serial_day_number - beg.serial_day_number

        def dcf(self, beg, end):
            return self.count_days(beg, end)/360.0

---

# Actual/360

* This convention is used in money markets for short-term lending of currencies.
* It is the convention used with Repurchase agreements. 

Each month is treated normally and the year is assumed to be 360 days. 

For example, in a period from February 1, 2005 to April 1, 2005, the Factor is 59 days divided by 360 days.

    !python
    class MeasureAct360(Measure):
        name = 'Act360'

    _measures[MeasureAct360.name] = MeasureAct360()

---

# Testing Actual/360

## Test all methods that calculate values

    !python
    import unittest
    import Measure
    import SerialDate

    d1 = SerialDate.make(20110202)
    d2 = SerialDate.make(20120202)

    class TestMeasure(unittest.TestCase):
        def test_act360_days(self):
            interval_days = Measure.get_dc('Act360').count_days(d1, d2)
            self.assertEqual(365.0, interval_days)

        def test_act360_dcf(self):
            dcf = Measure.get_dc('Act360').dcf(d1, d2)

            # be careful when comparing floating point numbers
            self.assertAlmostEqual(1.01388888889, dcf, 4)

---

# Operate on SerialDates

We can measure intervals. How can we generate dates?

## We need a SerialDateOperator API

    !python
    Operator.adjust(serial_date, adjustment)

## Example SerialDateOperators

* Days
* Months
* Years

---

# Serial Date Operators

    !python
    class SerialDateOperator(object):
        name = None

        def adjust(self, serial_date, adjustment):
            adjusted_day = serial_date.serial_day_number + adjustment
            return SerialDate.make(adjusted_day)

## Day Operator

    !python
    class DayOperator(SerialDateOperator):
        name = "DAY"

    _operators[DayOperator.name] = DayOperator()

## Month Operator

    !python
    class MonthOperator(SerialDateOperator):
        name = "MONTH"

        def adjust(self, serial_date, adjustment):
            dt = serial_date.as_datetime()
            rd = relativedelta(months = adjustment)
            dt = dt + rd
            return SerialDate.make(dt)

    _operators[MonthOperator.name] = MonthOperator()

---

# Serial Date Operator Tests

    !python
    base_date = SerialDate.make(20110202)

    class TestSerialDateOperator(unittest.TestCase):
        def test_day_operator(self):
            day_adjusted = SDO.get_operator('DAY').adjust(base_date, 30)
            self.assertEqual(40606.0, day_adjusted.serial_day_number)

        def test_month_operator(self):
            month_adjusted = SDO.get_operator('MONTH').adjust(base_date, 6)
            self.assertEqual(40757.0, month_adjusted.serial_day_number)

        def test_year_operator(self):
            year_adjusted = SDO.get_operator('YEAR').adjust(base_date, 10)
            self.assertEqual(44229.0, year_adjusted.serial_day_number)

---

# Conversion (in Class)

---

# Currency

---

# Discount Factor (and Curve)

---

# Payment

---

# Annuity

